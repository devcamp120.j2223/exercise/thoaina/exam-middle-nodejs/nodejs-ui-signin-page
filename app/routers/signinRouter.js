const express = require("express");
const { signinTimeMiddleware, signinURLMiddleware } = require("../middlewares/signinMiddleware");
const signinRouter = express.Router();

signinRouter.use(signinTimeMiddleware);
signinRouter.use(signinURLMiddleware);

// Khai báo thư viện path
const path = require("path");

// Khai báo sử dụng tài nguyên
signinRouter.use(express.static("views"));

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
signinRouter.get("/", (request, response) => {
    response.sendFile(path.join(__dirname + "/views/index.html"));
});

module.exports = signinRouter;