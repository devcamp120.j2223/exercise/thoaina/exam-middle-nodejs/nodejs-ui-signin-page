const signinTimeMiddleware = (req, res, next) => {
    let today = new Date();
    console.log(`Time Sign In: ${today.getDate()}`);
    next();
}

const signinURLMiddleware = (req, res, next) => {
    console.log(`URL Sign In: ${req.url}`);
    next();
}


module.exports = { signinTimeMiddleware, signinURLMiddleware };